from __future__ import annotations


def split_stoichiometries(s: dict[str, float]) -> tuple[list[str], list[str]]:
    subs = [k for k, v in s.items() if v < 0]
    prods = [k for k, v in s.items() if v > 0]
    return subs, prods
