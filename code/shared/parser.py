from __future__ import annotations
from dataclasses import dataclass
from enum import IntEnum, auto
from typing import Iterable


class TokenType(IntEnum):
    NUMBER = auto()
    NAME = auto()
    ARROW = auto()


@dataclass
class Token:
    token_type: TokenType
    content: str


@dataclass
class LexerState:
    file: str
    position: int = -1

    def peek(self) -> str | None:
        try:
            return self.file[self.position + 1]
        except IndexError:
            return None

    def advance(self) -> None:
        self.position += 1

    def get_next(self) -> str | None:
        token = self.peek()
        self.advance()
        return token


def lex(chars: str) -> Iterable[Token]:
    state = LexerState(chars)
    while (char := state.get_next()) is not None:
        if char.isspace():
            continue
        elif char == "+":
            continue
        elif char == "(":
            assert (num := state.get_next()) is not None
            assert state.get_next() == ")"
            yield Token(TokenType.NUMBER, num)
        elif char == "[":
            while state.get_next() != "]":
                continue
        elif char == "=":
            assert state.get_next() == ">"
            yield Token(TokenType.ARROW, "")
        elif char.isalpha():
            content = [char]
            while (c := state.peek()) is not None:
                if c == "[":
                    break
                else:
                    state.advance()
                    content.append(c)
            yield Token(TokenType.NAME, "".join(content))
        else:
            raise NotImplementedError(char)


def parse(tokens: Iterable[Token]) -> dict[str, int]:
    stoichiometries: dict[str, int] = {}
    factor = -1

    it = iter(tokens)
    while True:
        try:
            tok = next(it)
            if tok.token_type == TokenType.ARROW:
                factor = 1
            else:
                assert (value := tok).token_type == TokenType.NUMBER
                assert (name := next(it)).token_type == TokenType.NAME
                stoichiometries[name.content] = int(value.content) * factor
        except StopIteration:
            break
    return stoichiometries


if __name__ == "__main__":
    assert list(lex("(1)"))[0] == Token(TokenType.NUMBER, content="1")
    assert list(lex("cpd00911[c0]"))[0] == Token(TokenType.NAME, content="cpd00911")
    assert list(lex("=>"))[0] == Token(TokenType.ARROW, content="")
    assert list(lex("(1) cpd00911[c0] + (1) WATER[c0] => (1) pkc0000026[c0] + (1) pkc0000191[c0]")) == [
        Token(token_type=TokenType.NUMBER, content="1"),
        Token(token_type=TokenType.NAME, content="cpd00911"),
        Token(token_type=TokenType.NUMBER, content="1"),
        Token(token_type=TokenType.NAME, content="WATER"),
        Token(token_type=TokenType.ARROW, content=""),
        Token(token_type=TokenType.NUMBER, content="1"),
        Token(token_type=TokenType.NAME, content="pkc0000026"),
        Token(token_type=TokenType.NUMBER, content="1"),
        Token(token_type=TokenType.NAME, content="pkc0000191"),
    ]

    assert parse(lex("(1) cpd1[c0] => (1) cpd2[c0]")) == {
        "cpd1": -1,
        "cpd2": 1,
    }
    assert parse(lex("(1) cpd1[c0] + (1) cpd2[c0] => (1) cpd3[c0]")) == {
        "cpd1": -1,
        "cpd2": -1,
        "cpd3": 1,
    }
    assert parse(lex("(1) cpd1[c0] + (1) cpd2[c0] => (1) cpd3[c0] + (1) cpd4[c0]")) == {
        "cpd1": -1,
        "cpd2": -1,
        "cpd3": 1,
        "cpd4": 1,
    }
