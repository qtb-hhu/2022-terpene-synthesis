# %%
from __future__ import annotations

from dataclasses import dataclass
from queue import LifoQueue

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np


# %%
@dataclass
class Reaction:
    sub: str
    prod: str
    weight: float


compounds = {
    "S1",
    "S2",
    "I1",
    "I2",
    "I3",
    "I4",
    "P1",
    "P2",
}

reactions = {
    "v1": Reaction("S1", "I1", 0.1),
    "v2": Reaction("S2", "I2", 0.1),
    "v3": Reaction("I1", "I3", 0.2),
    "v4": Reaction("I2", "I3", 0.2),
    "v5": Reaction("I3", "P1", 0.3),
    "v6": Reaction("I2", "I4", 0.3),
    "v7": Reaction("I4", "P2", 0.1),
    # "v8": Reaction("I1", "I4", 0.1),
}

in_reaction = {cpd: {rxn_name for rxn_name, rxn in reactions.items() if cpd in rxn.sub} for cpd in compounds}


pos = {
    "S1": (0, 1),
    "S2": (0, 0),
    "I1": (1, 1),
    "I2": (1, 0),
    "I3": (2, 1),
    "I4": (2, 0),
    "P1": (3, 1),
    "P2": (3, 0),
}

g = nx.DiGraph()
g.add_nodes_from(compounds)
for k, rxn in reactions.items():
    g.add_edge(rxn.sub, rxn.prod, weight=rxn.weight, name=k)

weights = np.fromiter(nx.get_edge_attributes(g, "weight").values(), float)

nx.draw_networkx(
    g,
    pos,
    width=weights * 10,
    edge_color=weights,
    edge_cmap=plt.cm.viridis,  # type: ignore
)
nx.draw_networkx_edge_labels(g, pos, edge_labels=nx.get_edge_attributes(g, "name"))
plt.show()


# %%
def _deconstruct_path(end: str, parents: dict[str, tuple[str | None, str | None]]) -> list[str]:
    """Deconstruct the path taken."""
    met, rec = parents[end]
    assert met is not None
    assert rec is not None
    recs = [rec]
    while True:
        met, rec = parents[met]
        if met is not None and rec is not None:
            recs.append(rec)
        else:
            return recs[::-1]


def search(
    start_cpd_id: str,
    end_cpd_id: str,
    in_reaction: dict[str, set[str]],
    reactions: dict[str, Reaction],
    max_iterations: int = 1000,
) -> list[str] | None:
    q: LifoQueue[str] = LifoQueue()
    q.put(start_cpd_id)

    path: dict[str, tuple[str | None, str | None]] = {start_cpd_id: (None, None)}
    n = 0
    while not q.empty():
        substrate_id = q.get()
        if substrate_id == end_cpd_id:
            return _deconstruct_path(end_cpd_id, path)
        for rxn in in_reaction[substrate_id]:
            if (prod := reactions[rxn].prod) not in path:
                path[prod] = substrate_id, rxn
                q.put(prod)
        n += 1
        if n == max_iterations:
            return None
    return None


paths: dict[str, list[list[str]]] = {}
for s in ("S1", "S2"):
    for p in ("P1", "P2"):
        if (path := search(s, p, in_reaction, reactions)) is not None:
            paths.setdefault(p, list()).append(path)
print(paths)


# %%
# goal_distribution = {
#     "P1": 0.8,
#     "P2": 0.2,
# }


# weights = {
#     "v1": 0.1,
#     "v2": 0.1,
#     "v3": 0.2,
#     "v4": 0.2,
#     "v5": 0.3,
#     "v6": 0.3,
#     "v7": 0.1,
# }

# %%
